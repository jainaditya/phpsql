<?php
class foo
{
	private $private_bar = 'private';
	protected $protected_bar = 'protected';
	public $public_bar = 'public';
	public static $static_bar = 'static';

	private function private_echo()
	{
		echo $this->private_bar;
	}
	protected function protected_echo()
	{
		echo $this->protected_bar;
	}
	public function public_echo()
	{
		echo $this->public_bar;
	}
	public static function static_echo()
	{
		echo $this->static_bar;
	}
}
$myfoo = new foo();

//	INVALID: $this is not accessible in a static function.
//	$this refers to the current instance of the class and static method does not run off the instance.
//	$myfoo->static_echo();
	
//	VALID: Public functions can be access outside the class or by class object. It has no restrictions.
		$myfoo->public_echo();

//	INVALID: Protected functon can not be accessed outside the class. They can only be called within the class where it's defined or the class extending that class.
//	$myfoo->protected_echo();
	
//  INVALID: Private functions and variables can not be accessed outside the class. They can only be called within the class where they are defined.
//	$myfoo->private_bar = 'new private bar';
//	$myfoo->private_echo();

//	INVALID: $this is not accessible in a static function.
//	$this refers to the current instance of the class and static method does not run off the instance.
//	$myfoo::static_echo();
?>
