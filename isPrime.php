<?php

	/*
	* function check whether a number is prime or not 
	*/

	function isPrime($number) 
	{ 
		// 1 is not prime
		if ( $number <= 1 ) {
			return false;
		}
	  
		// Check from 2 to n-1 
		for ($i = 2; $i < $number; $i++) {
			if ($number % $i == 0) {
				return false; 
			}
		}
		return true; 
	} 
	/*
	* function to print primes 
	*/

	function printPrime($start, $end) 
	{ 
		for ($i = $start ; $i <= $end; $i++) { 
			if (isPrime($i))
				echo $i . " ";
		}
	}

	$start = 1;
	$end = 100;

	printPrime($start, $end);
?>