# The table has mutiple entry types and for every row there will be only one entry type. Entrytype should be it's own table.
# Also, table shouldn't have derived data for totalentry. It can be derived from sql query using COUNT.

# There will be a new table entry_type and giveaway_entry will have a foreign key entrytypeid. So the new table structure will be

CREATE TABLE `giveaway_entry` (
`entryid` int(10) unsigned NOT NULL auto_increment,
`giveawayid` int(10) unsigned NOT NULL,
`entrytypeid` int(10) unsigned NOT NULL,
`userid` int(10) unsigned NOT NULL,
`dateline` int(10) unsigned NOT NULL default 0,
`ipaddress` varchar(15) NOT NULL,
PRIMARY KEY (`entryid`),
UNIQUE KEY `giveawayid` (`giveawayid`,`userid`),
FOREIGN KEY (`entrytypeid`) references entry_type(id),
KEY `dateline` (`dateline`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# table entrytype
create table `entry_type` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`name` varchar(55) NOT NULL,
	PRIMARY KEY (`id`)
);

INSERT INTO `entry_type` (name)
	VALUES ("facebookentry"),
		("twitterentry"),
		("timespententry"),
		("entry");

INSERT INTO `giveaway_entry` SET
giveawayid = 1,
userid = 404,
entrytypeid = 1,
ipaddress = "10.10.10.10";

INSERT INTO `giveaway_entry` SET
giveawayid = 2,
userid = 404,
entrytypeid = 2,
ipaddress = "10.10.10.10";

INSERT INTO `giveaway_entry` SET
giveawayid = 1,
userid = 405,
entrytypeid = 2,
ipaddress = "10.10.10.11";




# Considering user can only have one entry per giveaway the following query will return the totalentry for a given user

SELECT count(entryid) as `totalentry`, userid FROM giveaway_entry 
GROUP BY userid
HAVING userid = 404;

# Query below will output totalentry for all the users
SELECT count(entryid) as `totalentry`, userid FROM giveaway_entry 
GROUP BY userid;
# LIMIT 20;

